﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework3
{
    public class DateTimeService : IDateTimeServiceTransient, IDateTimeServiceSingleton, IDateTimeServiceSingletonInstance, IDateTimeServiceScoped
    {
        private DateTime CreateDate { get; set; }
        public DateTimeService(DateTime? time = null)
        {
            CreateDate = time ?? DateTime.Now;
        }

        public string GetServiceCreateDate()
        {
            return $"Service was created at {CreateDate.ToString("dd/MM/yyyy hh:mm:ss")}" +
                $" but now the time is {DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")}";
        }
    }
}

﻿using System;

namespace Homework3
{
    public interface IDateTimeService
    {
        string GetServiceCreateDate();
    }

    public interface IDateTimeServiceTransient : IDateTimeService {}
    public interface IDateTimeServiceScoped : IDateTimeService {}
    public interface IDateTimeServiceSingleton : IDateTimeService {}
    public interface IDateTimeServiceSingletonInstance : IDateTimeService { }
}
﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Homework3
{
    class Program
    {
        static Dictionary<string, Type> LifestyleDictionary = new Dictionary<string, Type>
        {
            { "TRANSIENT", typeof(IDateTimeServiceTransient) },
            { "SINGLETON", typeof(IDateTimeServiceSingleton) },
            { "INSTANCE", typeof(IDateTimeServiceSingletonInstance) },
            { "SCOPED", typeof(IDateTimeServiceScoped) }
        };

        static void Main(string[] args)
        {
            var serviceProvidcer = new ServiceCollection()
                .AddTransient<IDateTimeServiceTransient, DateTimeService>()
                .AddSingleton<IDateTimeServiceSingleton, DateTimeService>()
                .AddSingleton<IDateTimeServiceSingletonInstance>(new DateTimeService(new DateTime(2008,10,1)))
                .AddScoped<IDateTimeServiceScoped, DateTimeService>()
                .BuildServiceProvider();

            foreach (var lifestyle in LifestyleDictionary)
            {
                Console.WriteLine($"{lifestyle.Key}:");

                var service = (DateTimeService)serviceProvidcer.GetService(lifestyle.Value);
                Console.WriteLine(service.GetServiceCreateDate());

                System.Threading.Thread.Sleep(3000);

                service = (DateTimeService)serviceProvidcer.GetService(lifestyle.Value);
                Console.WriteLine(service.GetServiceCreateDate());
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
